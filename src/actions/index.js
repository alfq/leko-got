import * as api from "../api";

export const FETCH_CHARACTERS_STARTED = "FETCH_CHARACTERS_STARTED";
export const FETCH_CHARACTERS_SUCCEEDED = "FETCH_CHARACTERS_SUCCEEDED";
export const FETCH_CHARACTERS_FAILED = "FETCH_CHARACTERS_FAILED";

export const CREATE_CHARACTER = "CREATE_CHARACTER";
export const CREATE_CHARACTER_SUCCEEDED = "CREATE_CHARACTER_SUCCEEDED";
export const CREATE_CHARACTER_FAILED = "CREATE_CHARACTER_FAILED";

export const DELETE_CHARACTER_SUCCEEDED = "DELETE_CHARACTER_SUCCEEDED";

export function fetchCharactersStarted() {
  return {
    type: FETCH_CHARACTERS_STARTED
  };
}

export function fetchCharactersSucceeded(characters) {
  return {
    type: FETCH_CHARACTERS_SUCCEEDED,
    payload: {
      characters
    }
  };
}

export function fetchCharactersFailed(error) {
  return {
    type: FETCH_CHARACTERS_FAILED,
    payload: {
      error
    }
  };
}

export function createCharacterSucceeded(character) {
  return {
    type: CREATE_CHARACTER_SUCCEEDED,
    payload: {
      character
    }
  };
}

export function createCharacterFailed(error) {
  return {
    type: CREATE_CHARACTER_FAILED,
    payload: {
      error
    }
  };
}

export function deleteCharacterSucceeded(id){
  return {
    type: DELETE_CHARACTER_SUCCEEDED,
    payload: {
      id
    }
  }
}

export function fetchCharacters() {
  return dispatch => {
    dispatch(fetchCharactersStarted());

    api
      .fetchCharacters()
      .then(res => {
        //Simulate 2s delay from getting data from server
        // so as to show "Loading...."
        // setTimeout(() => {
        dispatch(fetchCharactersSucceeded(res.data));
        // }, 2000);
      })
      .catch(err => {
        dispatch(fetchCharactersFailed(err.message));
      });
  };
}

export function createCharacter({ name, description, execution }) {
  return dispatch => {
    api
      .createCharacter({ name, description, execution })
      .then(res => {
        dispatch(createCharacterSucceeded);

        //Simulate an error
        // throw new Error("Some not so cool error");
      })
      .catch(err => {
        dispatch(createCharacterFailed(err.message));
      });
  };
}

export function deleteCharacter(id) {
  return dispatch => {
    api.deleteCharacter(id)
        .then(res => {
          dispatch(deleteCharacterSucceeded(id));
        })
  }
}
