import axios from "axios";

const API_BASE_URL = "http://localhost:3001";

const client = axios.create({
  baseURL: API_BASE_URL,
  headers: {
    "Content-Type": "application/json"
  }
});

export function fetchCharacters() {
  return client.get("/data");
}

export function createCharacter(params) {
  return client.post("/data", params);
}

export function deleteCharacter(id) {
  return client.delete(`${API_BASE_URL}/data/${id}`);
}
