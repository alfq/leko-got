import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import CharacterList from "./character_list";
import {connect} from "react-redux";

export class CharacterPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showNewCardForm: false
    };
  }

  onCreateCharacter = ({name, description, execution}) => {
    this.props.onCreateCharacter({name, description, execution})
    window.location.reload();
  };

  toggleForm = () => {
    this.setState({
      showNewCardForm: !this.state.showNewCardForm
    });
  };

  renderCharacters() {
    const { characters } = this.props;
    return <CharacterList onDeleteCharacter = {this.props.onDeleteCharacter} characters={characters} />;
  }

  render() {
    const { handleSubmit } = this.props;

    if(this.props.isLoading) return <h3>Loading....</h3>
    if(this.props.error) return <h3>Sorry, application error!</h3>

    return (
      <div>
        <div>
          <button className="btn btn-primary" onClick={this.toggleForm}>
            New Character
          </button>
        </div>
        {this.state.showNewCardForm &&
          <form onSubmit={handleSubmit(this.onCreateCharacter.bind(this))}>
          <Field
          label="Name"
          name="name"
          component = {this.renderField}
          ></Field>
          
          <Field
          label="Description"
          name = "description"
          component = {this.renderField}
          ></Field>

          <Field
          label = "Mode of execution"
          name = "execution"
          component = {this.renderField}
          ></Field>
          
          <button
            className="btn btn-success" type="submit">
            Save
          </button>
          </form>

        }
        <div>
        {this.renderCharacters()}
      </div>
      </div>
    )
  }

  renderField(field) {
    const { meta: { touched, error } } = field;

    const className = `form-group ${touched && error ? "has-danger" : ""}`;

    return (
      <div className={className}>
        <label>
          {field.label}
          <input
          {...field.input}
           type="text" 
           className="form-control" />
        </label>
        <div className="text-help">
          {touched ? error : ""}
        </div>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};
  if (!values.name) errors.name = "Name is required";
  if (!values.description) errors.description = "Description is required";
  if (!values.execution) errors.execution = "Mode of execution is required.";
  return errors;
}


export default reduxForm({
  validate,
  form: "create_character_form"
})(connect(null, {})(CharacterPage));
