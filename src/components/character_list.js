import React from "react";
import Character from "./character";

const CharacterList = props => {
  return (
    <div className="character-list">
      <h3>Characters</h3>
      <ul className="list-group">
        {props.characters.map(character => (
          <li key={character.id} className="list-group-item">
            <Character character={character} />
            <button
            onClick = {()=>props.onDeleteCharacter(character.id)}
            className="btn btn-danger">Delete</button>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default CharacterList;
