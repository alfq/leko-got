import {
  FETCH_CHARACTERS_STARTED,
  FETCH_CHARACTERS_SUCCEEDED,
  CREATE_CHARACTER_SUCCEEDED,
  CREATE_CHARACTER_FAILED,
  DELETE_CHARACTER_SUCCEEDED
} from "../actions";

const initialState = {
  characters: [],
  isLoading: false,
  error: null
};

export default function characters(state = initialState, action) {
  switch (action.type) {
    case FETCH_CHARACTERS_STARTED:
      return { ...state, isLoading: true };

    case FETCH_CHARACTERS_SUCCEEDED:
      return {
        ...state,
        isLoading: false,
        characters: state.characters.concat(action.payload.characters)
      };

    case CREATE_CHARACTER_SUCCEEDED:
      return {
        ...state,
        characters: state.characters.concat(action.payload.characters)
      }

    case CREATE_CHARACTER_FAILED:
      return {
        ...state,
        error: action.payload.error
      }

    case DELETE_CHARACTER_SUCCEEDED: 
      const characters = state.characters.filter(character => character.id !== action.payload.id);
      return {characters}

    default:
      return state;
  }
}
