import React, { Component } from "react";
import { connect } from "react-redux";
import CharacterPage from "./components/character_page";
import "./App.css";
import { fetchCharacters, createCharacter, deleteCharacter } from "./actions/index";

class App extends Component {
  componentDidMount() {
    this.props.dispatch(fetchCharacters());
    this.onCreateCharacter = this.onCreateCharacter.bind(this);
    this.onDeleteCharacter = this.onDeleteCharacter.bind(this);
  }

  onCreateCharacter({name, description, execution}){
    console.log("Hey, here")
    this.props.dispatch(createCharacter({name, description, execution}))
    this.forceUpdate();
  }

  onDeleteCharacter(id) {
    this.props.dispatch(deleteCharacter(id))
  }

  render() {
    return (
      <div className="container">
        <CharacterPage
          isLoading={this.props.isLoading}
          characters={this.props.characters}
          onCreateCharacter = {this.onCreateCharacter}
          onDeleteCharacter = {this.onDeleteCharacter}
          error = {this.props.error}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { characters, isLoading, error } = state.characters;
  return {
    characters,
    isLoading,
    error
  };
}

export default connect(mapStateToProps)(App);
